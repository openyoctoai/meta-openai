SUMMARY = "Open-AI production image"

IMAGE_FEATURES += "splash"

LICENSE = "MIT"

require openai-image-base.bb

inherit extrausers

EXTRA_USERS_PARAMS = "usermod -P root root;"

IMAGE_FEATURES += "ssh-server-openssh tools-debug debug-tweaks"

CORE_IMAGE_EXTRA_INSTALL += "ethtool evtest fbset i2c-tools memtester"

IMAGE_INSTALL_append = " \
        packagegroup-core-boot \
        openssh-sftp-server \
        "
