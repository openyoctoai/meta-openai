SUMMARY = "Open-AI production image"

IMAGE_FEATURES += "splash"

LICENSE = "MIT"

inherit core-image

CORE_IMAGE_EXTRA_INSTALL = "inotify-tools"

PYTHON_PKG = " \
    python3 \
    "

OPENCV_PKG = " \
    opencv \
    "

IMAGE_INSTALL_append = " \
    ${PYTHON_PKG} \
    ${OPENCV_PKG} \
    "
