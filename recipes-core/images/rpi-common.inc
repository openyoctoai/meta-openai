# Include modules in rootfs
IMAGE_INSTALL += " \
	kernel-modules \
	"

IMAGE_INSTALL_append = " \
	init-ip-raspberrypi \	
	"

SDIMG_ROOTFS_TYPE = "ext4"
